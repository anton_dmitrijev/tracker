package com.bsc;

import com.bsc.domain.BalanceRecord;
import com.bsc.domain.BalanceSheet;
import com.bsc.domain.Transaction;
import com.bsc.service.TransactionRecordService;
import org.junit.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static org.assertj.core.api.Assertions.assertThat;

public class InMemoryTransactionRecordServiceTest {
    private TransactionRecordService service = new InMemoryTransactionRecordService();

    @Test
    public void addOneTransaction_shouldNotProduceAnyException() {
        service.addTransaction(new Transaction("USD", TEN));
    }

    @Test
    public void whenOneTransactionWasAdded_balanceSheetShouldContainOneRecord() {
        service.addTransaction(new Transaction("USD", TEN));

        BalanceSheet balanceSheet = service.getBalanceSheet();

        assertThat(balanceSheet.getRecords())
                .hasSize(1)
                .containsExactly(new BalanceRecord("USD", TEN));
    }

    @Test
    public void whenSecondTransactionForTheSameCurrencyAdded_balanceWillContainSumOfThose() {
        service.addTransaction(new Transaction("USD", TEN));
        service.addTransaction(new Transaction("USD", ONE));

        BalanceSheet balanceSheet = service.getBalanceSheet();

        assertThat(balanceSheet.getRecords())
                .hasSize(1)
                .containsExactly(new BalanceRecord("USD", new BigDecimal("11")));
    }

    @Test
    public void whenTransactionWithNegativeValueIsAdded_sumOfTheseTransactionWillBeTheBalance() {
        service.addTransaction(new Transaction("USD", TEN));
        service.addTransaction(new Transaction("USD", new BigDecimal("-5")));

        BalanceSheet balanceSheet = service.getBalanceSheet();

        assertThat(balanceSheet.getRecords())
                .hasSize(1)
                .containsExactly(new BalanceRecord("USD", new BigDecimal("5")));
    }

    @Test
    public void whenBalanceIsExactlyZero_currencyWillNotShowUpInTheBalanceSheet() {
        service.addTransaction(new Transaction("USD", TEN));
        service.addTransaction(new Transaction("USD", new BigDecimal("-10")));

        BalanceSheet balanceSheet = service.getBalanceSheet();

        assertThat(balanceSheet.getRecords()).isEmpty();
    }

    @Test
    public void whenBalanceForCurrencyIsMoreThanZero_currencyIsIncludedIntoTheSheet() {
        service.addTransaction(new Transaction("USD", TEN));
        service.addTransaction(new Transaction("USD", new BigDecimal("-9.99")));

        BalanceSheet balanceSheet = service.getBalanceSheet();

        assertThat(balanceSheet.getRecords())
                .hasSize(1)
                .containsExactly(new BalanceRecord("USD", new BigDecimal("0.01")));
    }

    @Test
    public void whenBalanceForCurrencyIsABitLessThanZero_currencyIsIncludedIntoTheSheet() {
        service.addTransaction(new Transaction("USD", TEN));
        service.addTransaction(new Transaction("USD", new BigDecimal("-10.01")));

        BalanceSheet balanceSheet = service.getBalanceSheet();

        assertThat(balanceSheet.getRecords())
                .hasSize(1)
                .containsExactly(new BalanceRecord("USD", new BigDecimal("-0.01")));
    }

    @Test
    public void whenTransactionWithOtherCurrencyIsAdded_balanceSheetWillContainTwoCurrencies() {
        service.addTransaction(new Transaction("USD", TEN));
        service.addTransaction(new Transaction("EUR", TEN));

        BalanceSheet balanceSheet = service.getBalanceSheet();

        assertThat(balanceSheet.getRecords())
                .hasSize(2)
                .containsOnly(new BalanceRecord("USD", TEN), new BalanceRecord("EUR", TEN));
    }

    @Test
    public void whenThereAreTwoCurrenciesAndSumOfOneOfThemIsZero_showOtherCurrencyInTheSheet() {
        service.addTransaction(new Transaction("USD", TEN));
        service.addTransaction(new Transaction("USD", new BigDecimal("-10")));
        service.addTransaction(new Transaction("EUR", TEN));

        BalanceSheet balanceSheet = service.getBalanceSheet();

        assertThat(balanceSheet.getRecords())
                .hasSize(1)
                .containsOnly(new BalanceRecord("EUR", TEN));
    }
}