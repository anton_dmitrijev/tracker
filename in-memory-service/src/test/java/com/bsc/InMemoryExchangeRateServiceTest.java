package com.bsc;

import com.bsc.domain.ExchangeRate;
import com.bsc.service.ExchangeRateService;
import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class InMemoryExchangeRateServiceTest {
    private ExchangeRateService service = new InMemoryExchangeRateService();

    @Test
    public void addAndGetRate() {
        ExchangeRate expectedRate = new ExchangeRate("CZK", "USD", new BigDecimal("24.95"));
        service.addRate(expectedRate);

        assertThat(service.getRate("CZK", "USD")).isEqualTo(expectedRate);
    }

    @Test
    public void addTwoRates_getTheRightOneWhenRequested() {
        ExchangeRate rate1 = new ExchangeRate("CZK", "USD", new BigDecimal("24.95"));
        ExchangeRate rate2 = new ExchangeRate("GBP", "USD", new BigDecimal("0.77"));

        service.addRate(rate1);
        service.addRate(rate2);

        assertThat(service.getRate("CZK", "USD")).isEqualTo(rate1);
        assertThat(service.getRate("GBP", "USD")).isEqualTo(rate2);
    }
}