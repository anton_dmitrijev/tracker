package com.bsc;

import com.bsc.domain.ExchangeRate;
import com.bsc.service.ExchangeRateService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryExchangeRateService implements ExchangeRateService {
    private final Map<String, ExchangeRate> rates = new ConcurrentHashMap<>();

    @Override
    public void addRate(ExchangeRate rate) {
        rates.put(createKey(rate.getFrom(), rate.getTo()), rate);
    }

    @Override
    public ExchangeRate getRate(String from, String to) {
        return rates.get(createKey(from, to));
    }

    private String createKey(String from, String to) {
        return from + "/" + to;
    }
}
