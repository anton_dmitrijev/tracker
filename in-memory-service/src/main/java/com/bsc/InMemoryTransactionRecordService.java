package com.bsc;

import com.bsc.domain.BalanceRecord;
import com.bsc.domain.BalanceSheet;
import com.bsc.domain.Transaction;
import com.bsc.service.TransactionRecordService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

import static java.math.BigDecimal.ZERO;
import static java.util.Collections.emptyEnumeration;
import static java.util.Collections.synchronizedList;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.reducing;
import static java.util.stream.Collectors.summingLong;
import static java.util.stream.Collectors.toList;

public class InMemoryTransactionRecordService implements TransactionRecordService {

    private final Collection<Transaction> transactions = synchronizedList(new ArrayList<>());

    @Override
    public void addTransaction(Transaction transaction) {
        transactions.add(transaction);
    }

    @Override
    public BalanceSheet getBalanceSheet() {
        Collection<BalanceRecord> accounts = transactions.stream()
                .collect(
                        groupingBy(
                                Transaction::getCurrency,
                                reducing(ZERO, Transaction::getAmount, BigDecimal::add)
                        )
                )
                .entrySet().stream()
                .map(e -> new BalanceRecord(e.getKey(), e.getValue()))
                .filter(record -> !ZERO.equals(record.getTotalSum()))
                .collect(toList());
        return new BalanceSheet(accounts);
    }
}
