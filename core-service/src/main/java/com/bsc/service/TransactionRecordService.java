package com.bsc.service;

import com.bsc.domain.BalanceSheet;
import com.bsc.domain.Transaction;

public interface TransactionRecordService {
    void addTransaction(Transaction transaction);

    BalanceSheet getBalanceSheet();
}
