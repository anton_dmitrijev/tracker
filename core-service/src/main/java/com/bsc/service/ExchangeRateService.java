package com.bsc.service;

import com.bsc.domain.ExchangeRate;

public interface ExchangeRateService {

    void addRate(ExchangeRate rate);

    ExchangeRate getRate(String from, String to);
}
