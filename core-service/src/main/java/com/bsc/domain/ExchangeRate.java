package com.bsc.domain;

import java.math.BigDecimal;

public final class ExchangeRate {
    private final String from;
    private final String to;
    private final BigDecimal rate;

    public ExchangeRate(String from, String to, BigDecimal rate) {
        this.from = from;
        this.to = to;
        this.rate = rate;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExchangeRate that = (ExchangeRate) o;

        return !(from != null ? !from.equals(that.from) : that.from != null)
                && !(rate != null ? !rate.equals(that.rate) : that.rate != null)
                && !(to != null ? !to.equals(that.to) : that.to != null);
    }

    @Override
    public int hashCode() {
        int result = from != null ? from.hashCode() : 0;
        result = 31 * result + (to != null ? to.hashCode() : 0);
        result = 31 * result + (rate != null ? rate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return from + '/' + to + " " + rate;
    }
}
