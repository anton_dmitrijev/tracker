package com.bsc.domain;

import java.util.Collection;
import java.util.Collections;

public final class BalanceSheet {
    private final Collection<BalanceRecord> records;

    public BalanceSheet(Collection<BalanceRecord> records) {
        this.records = Collections.unmodifiableCollection(records);
    }

    public Collection<BalanceRecord> getRecords() {
        return records;
    }
}
