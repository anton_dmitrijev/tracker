package com.bsc.domain;

import java.math.BigDecimal;

public final class BalanceRecord {
    private final String currency;
    private final BigDecimal totalSum;

    public BalanceRecord(String currency, BigDecimal totalSum) {
        this.currency = currency;
        this.totalSum = totalSum;
    }

    public String getCurrency() {
        return currency;
    }

    public BigDecimal getTotalSum() {
        return totalSum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BalanceRecord that = (BalanceRecord) o;

        return currency.equals(that.currency)
                && totalSum.equals(that.totalSum);
    }

    @Override
    public int hashCode() {
        int result = currency.hashCode();
        result = 31 * result + totalSum.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return totalSum + " " + currency;
    }
}
