Disclaimer: As the exercise required a use of design patterns this solution is over engineered intentionally ;)

How to run the app

Run these commands in order (java 8 is expected):

mvn clean install
cd app
mvn exec:java

To provide a path to transactions file use `transactionsFilePath` flag
e.g. mvn exec:java -DtransactionsFilePath=%PATH TO TRANSACTIONS FILE%
An absolute path is expected

Accepted commands are:
Currency Amount             -   e.g. USD 500.
                                Currency    - expected to be an upper-case 3 letter currency code.
                                Amount      - can be a positive/negative decimal number. Separator is a dot(.)
Currency1 Currency2 Rate    -   e.g. CZK USD 1.23
                                Currency1   - currency to sell
                                Currency2   - currency to buy
                                Rate        - a positive decimal number. Separator is a dot (.)
quit                        -   Exit the program
