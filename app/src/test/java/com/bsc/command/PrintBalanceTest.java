package com.bsc.command;

import com.bsc.domain.BalanceRecord;
import com.bsc.domain.BalanceSheet;
import com.bsc.domain.ExchangeRate;
import com.bsc.service.ExchangeRateService;
import com.bsc.service.TransactionRecordService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PrintBalanceTest {
    @Mock
    private TransactionRecordService transactionRecordService;
    @Mock
    private ExchangeRateService exchangeRateService;
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void onExecute_ifNoTransactionsExist_doNothing() {
        printBalanceForTransactions(Collections.<BalanceRecord>emptyList());

        assertThat(systemOutRule.getLog()).isEmpty();
    }

    @Test
    public void onExecute_printOneRecord() {
        printBalanceForTransactions(asList(new BalanceRecord("USD", BigDecimal.TEN)));

        assertThat(systemOutRule.getLog()).isEqualTo("\nBalance:\nUSD 10\n");
    }

    @Test
    public void onExecute_printTwoRecords() {
        printBalanceForTransactions(asList(
                new BalanceRecord("EUR", BigDecimal.ONE),
                new BalanceRecord("CZK", new BigDecimal("100.50"))
        ));

        assertThat(systemOutRule.getLog()).isEqualTo("\nBalance:\nEUR 1\nCZK 100.50\n");
    }

    @Test
    public void onExecute_ifCurrencyRateIsPresent_provideItInOutput() {
        when(exchangeRateService.getRate("CZK", "USD")).thenReturn(new ExchangeRate("CZK", "USD", new BigDecimal("24")));
        printBalanceForTransactions(asList(
                        new BalanceRecord("CZK", new BigDecimal("100")),
                        new BalanceRecord("EUR", new BigDecimal("500"))
                )
        );

        assertThat(systemOutRule.getLog()).isEqualTo("\nBalance:\nCZK 100 (4.17 USD)\nEUR 500\n");
    }

    private void printBalanceForTransactions(List<BalanceRecord> records) {
        when(transactionRecordService.getBalanceSheet()).thenReturn(new BalanceSheet(records));

        new PrintBalance(transactionRecordService, exchangeRateService).execute();
    }
}