package com.bsc.command;

import com.bsc.domain.Transaction;
import com.bsc.service.TransactionRecordService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class AddTransactionsFromFileTest {
    @Mock
    private TransactionRecordService service;
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void importFileWithNoTransactions_balanceWillHaveNoTransactions() throws IOException {
        importLines(Collections.<String>emptyList());

        Mockito.verifyZeroInteractions(service);
    }

    @Test
    public void importFileWithOneTransaction_balanceWillHaveThisTransaction() throws IOException {
        importLines(Collections.singletonList("USD 100.11"));

        verify(service).addTransaction(new Transaction("USD", new BigDecimal("100.11")));
    }

    @Test
    public void importFileWithTwoTransactions_balanceWillContainImportedTransactions() throws IOException {
        importLines(asList("USD 2.05", "EUR 300"));

        verify(service).addTransaction(new Transaction("USD", new BigDecimal("2.05")));
        verify(service).addTransaction(new Transaction("EUR", new BigDecimal("300")));
    }

    @Test
    public void importTwoTransactionsWithFaultyRows_validTransactionsWillBeIncludedIntoBalanceSheet() throws IOException {
        importLines(asList("USD 3", "CZK 6000", "USD CZK"));

        verify(service).addTransaction(new Transaction("USD", new BigDecimal("3")));
        verify(service).addTransaction(new Transaction("CZK", new BigDecimal("6000")));
        verifyNoMoreInteractions(service);
    }

    @Test
    public void importOnlyFaultyTransactions_noTransactionsWillBeInTheBalance() throws IOException {
        importLines(asList("100 USD", "100000 CZK"));

        verifyZeroInteractions(service);
    }

    @Test
    public void ifFileDoesNotExist_doNothing() {
        new AddTransactionsFromFile("some-non-existing-file", service).execute();

        verifyZeroInteractions(service);
    }

    @Test
    public void ifPathIsNull_doNothing() {
        new AddTransactionsFromFile(null, service).execute();

        verifyZeroInteractions(service);
    }

    private void importLines(List<String> lines) throws IOException {
        Path path = writeToTempFile(lines);
        new AddTransactionsFromFile(path.toString(), service).execute();
    }

    private Path writeToTempFile(Collection<String> lines) throws IOException {
        Path path = temporaryFolder.newFile().toPath();
        Files.write(path, lines);
        return path;
    }
}