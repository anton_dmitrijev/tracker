package com.bsc.command;

import com.bsc.domain.Transaction;
import com.bsc.service.TransactionRecordService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AddTransactionTest {
    @Mock
    private TransactionRecordService service;

    @Test
    public void onExecute_addTransactionToTracker() {
        Transaction transaction = new Transaction("USD", BigDecimal.TEN);

        new AddTransaction(service, transaction).execute();

        verify(service).addTransaction(transaction);
    }
}