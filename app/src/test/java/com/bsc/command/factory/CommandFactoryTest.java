package com.bsc.command.factory;

import com.bsc.command.AddExchangeRate;
import com.bsc.command.AddTransaction;
import com.bsc.command.AddTransactionsFromFile;
import com.bsc.command.Command;
import com.bsc.command.PrintBalance;
import com.bsc.command.QuitCommand;
import com.bsc.command.UnknownCommand;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static com.bsc.command.factory.CommandFactory.CommandType.CONSOLE_INPUT;
import static com.bsc.command.factory.CommandFactory.CommandType.IMPORT_FILE;
import static com.bsc.command.factory.CommandFactory.CommandType.PRINT_BALANCE;
import static com.bsc.util.TransactionUtil.parseTransaction;
import static org.assertj.core.api.Assertions.assertThat;

public class CommandFactoryTest {
    private CommandFactory factory = new CommandFactory();
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void createImportFileCommand() {
        Command cmd = factory.createCommand(IMPORT_FILE, "path");

        assertThat(cmd).isInstanceOf(AddTransactionsFromFile.class);
    }

    @Test
    public void createPrintBalanceCommand() {
        Command cmd = factory.createCommand(PRINT_BALANCE, "any input");

        assertThat(cmd).isInstanceOf(PrintBalance.class);
    }

    @Test
    public void createQuitCommand() {
        Command cmd = factory.createCommand(CONSOLE_INPUT, "quit");

        assertThat(cmd).isInstanceOf(QuitCommand.class);
    }

    @Test
    public void noTypeProvided() {
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("No commands relate to type=null");

        Command cmd = factory.createCommand(null, "USD 100");

        assertThat(cmd).isNull();
    }

    @Test
    public void createAddTransactionCommand() {
        String transactionLine = "USD 300";
        Command cmd = factory.createCommand(CONSOLE_INPUT, transactionLine);

        assertThat(cmd).isInstanceOf(AddTransaction.class);
        assertThat(((AddTransaction) cmd).getTransaction())
                .isEqualTo(parseTransaction(transactionLine));
    }

    @Test
    public void createUnknownCommand() {
        Command cmd = factory.createCommand(CONSOLE_INPUT, "300 USD");

        assertThat(cmd).isInstanceOf(UnknownCommand.class);
    }

    @Test
    public void createExchangeRateCommand() {
        Command cmd = factory.createCommand(CONSOLE_INPUT, "GBP USD 0.8");

        assertThat(cmd).isInstanceOf(AddExchangeRate.class);
    }
}