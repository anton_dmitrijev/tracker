package com.bsc.command.executor;

import com.bsc.command.AddTransaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CommandExecutorTest {
    @Mock
    private AddTransaction transaction;

    @Test
    public void executeIncomingCommand() {
        CommandExecutor commandExecutor = new CommandExecutor();

        commandExecutor.executeCommand(transaction);

        Mockito.verify(transaction).execute();
    }
}