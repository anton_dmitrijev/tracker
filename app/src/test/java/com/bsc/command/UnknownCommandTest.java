package com.bsc.command;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import static org.assertj.core.api.Assertions.assertThat;

public class UnknownCommandTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void onExecute_printErrorMessage() {
        new UnknownCommand().execute();

        assertThat(systemOutRule.getLog()).contains("Invalid input!");
    }
}