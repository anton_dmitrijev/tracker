package com.bsc.command;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

public class QuitCommandTest {
    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Test
    public void onExecute_quitTheProgram() {
        exit.expectSystemExitWithStatus(0);

        new QuitCommand().execute();
    }
}