package com.bsc.command;

import com.bsc.domain.ExchangeRate;
import com.bsc.service.ExchangeRateService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AddExchangeRateTest {
    @Mock
    private ExchangeRateService service;

    @Test
    public void onExecute_saveExchangeRate() {
        ExchangeRate exchangeRate = new ExchangeRate("CZK", "USD", new BigDecimal("24.98"));
        AddExchangeRate rate = new AddExchangeRate(service, exchangeRate);

        rate.execute();

        verify(service).addRate(exchangeRate);
    }
}