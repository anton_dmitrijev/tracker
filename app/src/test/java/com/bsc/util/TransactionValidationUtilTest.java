package com.bsc.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static com.bsc.util.TransactionUtil.isValidTransactionImportLine;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class TransactionValidationUtilTest {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"USD 100", true},
                {"USD 1", true},
                {"USD 0.99", true},
                {"USD 0.9", true},
                {"USD 0.01", true},
                {"USD -0.00", true},
                {"USD -10", true},
                {"USD -10.0", true},
                {"USD -10.00", true},
                {"EUR 100.1", true},
                {"EUR 100.11", true},
                {"EUR 10.", false},
                {"EU .99", false},
                {"EU 100", false},
                {"EU 10.", false},
                {"100 EUR", false},
                {"100.01 EUR", false},
                {"100.1 EUR", false}
        });
    }

    private String line;
    private boolean valid;

    public TransactionValidationUtilTest(String line, boolean valid) {
        this.line = line;
        this.valid = valid;
    }

    @Test
    public void validateTransaction() {
        assertThat(isValidTransactionImportLine(line))
                .isEqualTo(valid);
    }
}