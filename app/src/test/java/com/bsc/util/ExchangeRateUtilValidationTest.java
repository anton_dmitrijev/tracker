package com.bsc.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class ExchangeRateUtilValidationTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"HUF USD 300", true},
                {"CZK USD 24", true},
                {"CZK USD 1", true},
                {"GBP USD 1.0", true},
                {"CZK GBP 1.01", true},
                {"CZK USD 0.1", true},
                {"CZK USD 0", false},
                {"CZK GBP -1", false},
                {"CZK GBP -1.0", false},
                {"CZK GBP -1.01", false},
                {"cZK GBP 1.01", false},
                {"czK GBP 1.01", false},
                {"czk GBP 1.01", false},
                {"czk gBP 1.01", false},
                {"czk gbP 1.01", false},
                {"czk gbp 1.01", false},
                {"CZ GB 1.01", false},
                {"CZ GP 1.01", false}
        });
    }

    private String command;
    private boolean valid;

    public ExchangeRateUtilValidationTest(String command, boolean valid) {
        this.command = command;
        this.valid = valid;
    }

    @Test
    public void validateExchangeRate() {
        assertThat(ExchangeRateUtil.isValidExchangeRateCommand(command))
                .isEqualTo(valid);
    }
}