package com.bsc.util;

import com.bsc.domain.Transaction;
import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class TransactionUtilTest {
    @Test
    public void ifTransactionIsInvalid_returnNull() {
        Transaction transaction = TransactionUtil.parseTransaction("100 USD");

        assertThat(transaction).isNull();
    }

    @Test
    public void createTransaction() {
        Transaction transaction = TransactionUtil.parseTransaction("USD 100");

        assertThat(transaction)
                .isEqualTo(new Transaction("USD", new BigDecimal("100")));
    }
}