package com.bsc.util;

import com.bsc.domain.ExchangeRate;
import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;


public class ExchangeRateUtilTest {
    @Test
    public void parseExchangeRate() {
        String command = "CZK USD 24";

        ExchangeRate rate = ExchangeRateUtil.parseExchangeRate(command);

        assertThat(rate).isEqualTo(new ExchangeRate("CZK", "USD", new BigDecimal("24")));
    }

    @Test
    public void whenCommandIsInvalid_returnNull() {
        String command = "CZK USD -2";

        ExchangeRate rate = ExchangeRateUtil.parseExchangeRate(command);

        assertThat(rate).isNull();
    }
}