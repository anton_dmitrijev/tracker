package com.bsc.di;

import com.bsc.Application;
import com.bsc.InMemoryExchangeRateService;
import com.bsc.InMemoryTransactionRecordService;
import com.bsc.command.executor.CommandExecutor;
import com.bsc.service.ExchangeRateService;
import com.bsc.service.TransactionRecordService;
import dagger.Module;
import dagger.Provides;

@Module(
        injects = Application.class
)
public class ApplicationModule {
    @Provides
    TransactionRecordService provideTransactionRecordService() {
        return new InMemoryTransactionRecordService();
    }

    @Provides
    ExchangeRateService provideExchangeRateService() {
        return new InMemoryExchangeRateService();
    }

    @Provides
    CommandExecutor provideCommandExecutor() {
        return new CommandExecutor();
    }
}
