package com.bsc.command;

import com.bsc.service.TransactionRecordService;
import com.bsc.util.TransactionUtil;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public class AddTransactionsFromFile implements Command {
    private final String path;
    private TransactionRecordService service;

    public AddTransactionsFromFile(String path, TransactionRecordService service) {
        this.path = path;
        this.service = service;
    }

    @Override
    public void execute() {
        if (path == null) {
            return;
        }
        Path fileForImport = FileSystems.getDefault().getPath(path);
        if (!Files.exists(fileForImport)) {
            return;
        }
        try {
            Files.lines(fileForImport)
                    .filter(TransactionUtil::isValidTransactionImportLine)
                    .map(TransactionUtil::parseTransaction)
                    .forEach(service::addTransaction);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to load the transaction import file=" + path);
        }
    }
}
