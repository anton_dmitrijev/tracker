package com.bsc.command;

public class UnknownCommand implements Command {
    @Override
    public void execute() {
        System.out.println("Invalid input!\n" +
                "Available commands are:\n" +
                "CCY AMT - where CCY is a 3 character currency code and AMT is a transaction sum in format d+.dd\n" +
                "CCY CCY RATE - where CCY is a 3 character currency code and RATE is an exchange rate presented as positive decimal d+.dd\n" +
                "quit - quit the application\n");
    }
}
