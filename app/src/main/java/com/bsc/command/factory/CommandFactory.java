package com.bsc.command.factory;

import com.bsc.command.AddExchangeRate;
import com.bsc.command.AddTransaction;
import com.bsc.command.AddTransactionsFromFile;
import com.bsc.command.Command;
import com.bsc.command.PrintBalance;
import com.bsc.command.QuitCommand;
import com.bsc.command.UnknownCommand;
import com.bsc.service.ExchangeRateService;
import com.bsc.service.TransactionRecordService;
import com.bsc.util.ExchangeRateUtil;

import javax.inject.Inject;

import static com.bsc.util.ExchangeRateUtil.parseExchangeRate;
import static com.bsc.util.TransactionUtil.isValidTransactionImportLine;
import static com.bsc.util.TransactionUtil.parseTransaction;

public class CommandFactory {

    @Inject
    TransactionRecordService transactionRecordService;
    @Inject
    ExchangeRateService exchangeRateService;

    public static enum CommandType {
        IMPORT_FILE, CONSOLE_INPUT, PRINT_BALANCE
    }

    public Command createCommand(CommandType type, String argument) {
        if (CommandType.IMPORT_FILE == type) {
            return createImportFileCommand(argument);
        }
        if (CommandType.CONSOLE_INPUT == type) {
            return createConsoleCommand(argument);
        }
        if (CommandType.PRINT_BALANCE == type) {
            return printBalanceCommand();
        }
        throw new IllegalStateException("No commands relate to type=" + type + ", argument=" + argument);
    }

    private Command printBalanceCommand() {
        return new PrintBalance(transactionRecordService, exchangeRateService);
    }

    private Command createConsoleCommand(String command) {
        if (command.equals("quit")) {
            return new QuitCommand();
        }
        if (isValidTransactionImportLine(command)) {
            return new AddTransaction(transactionRecordService, parseTransaction(command));
        }
        if (ExchangeRateUtil.isValidExchangeRateCommand(command)) {
            return new AddExchangeRate(exchangeRateService, parseExchangeRate(command));
        }
        return new UnknownCommand();
    }

    private Command createImportFileCommand(String argument) {
        return new AddTransactionsFromFile(argument, transactionRecordService);
    }
}
