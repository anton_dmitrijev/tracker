package com.bsc.command;

import com.bsc.domain.BalanceRecord;
import com.bsc.domain.BalanceSheet;
import com.bsc.domain.ExchangeRate;
import com.bsc.service.ExchangeRateService;
import com.bsc.service.TransactionRecordService;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.stream.Collectors;

public class PrintBalance implements Command {
    private final TransactionRecordService transactionRecordService;
    private final ExchangeRateService exchangeRateService;

    public PrintBalance(TransactionRecordService transactionRecordService,
                        ExchangeRateService exchangeRateService) {
        this.transactionRecordService = transactionRecordService;
        this.exchangeRateService = exchangeRateService;
    }

    @Override
    public void execute() {
        BalanceSheet balanceSheet = transactionRecordService.getBalanceSheet();
        Collection<BalanceRecord> records = balanceSheet.getRecords();
        if (records.isEmpty()) {
            return;
        }
        String balance = records.stream()
                .map(record -> record.getCurrency() + " " + record.getTotalSum()
                                + provideUsdEquivalentIfPossible(record)
                )
                .collect(Collectors.joining("\n"));

        System.out.println(("\nBalance:\n" + balance));
    }

    private String provideUsdEquivalentIfPossible(BalanceRecord record) {
        ExchangeRate rate = exchangeRateService.getRate(record.getCurrency(), "USD");
        if (rate == null) {
            return "";
        }
        BigDecimal usdEquivalent =
                record.getTotalSum().divide(rate.getRate(), 2, BigDecimal.ROUND_HALF_UP);
        return " (" + usdEquivalent + " USD)";
    }
}
