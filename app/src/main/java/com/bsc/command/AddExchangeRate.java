package com.bsc.command;

import com.bsc.domain.ExchangeRate;
import com.bsc.service.ExchangeRateService;

public class AddExchangeRate implements Command {
    private final ExchangeRate exchangeRate;
    private final ExchangeRateService exchangeRateService;

    public AddExchangeRate(ExchangeRateService exchangeRateService, ExchangeRate exchangeRate) {
        this.exchangeRate = exchangeRate;
        this.exchangeRateService = exchangeRateService;
    }

    @Override
    public void execute() {
        exchangeRateService.addRate(exchangeRate);
    }
}
