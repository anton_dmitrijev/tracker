package com.bsc.command.executor;

import com.bsc.command.Command;

public class CommandExecutor {
    public void executeCommand(Command cmd) {
        cmd.execute();
    }
}
