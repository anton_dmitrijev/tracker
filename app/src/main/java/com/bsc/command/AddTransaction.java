package com.bsc.command;

import com.bsc.domain.Transaction;
import com.bsc.service.TransactionRecordService;

public class AddTransaction implements Command {
    private final TransactionRecordService service;
    private final Transaction transaction;

    public AddTransaction(TransactionRecordService service, Transaction transaction) {
        this.service = service;
        this.transaction = transaction;
    }

    @Override
    public void execute() {
        service.addTransaction(transaction);
    }

    public Transaction getTransaction() {
        return transaction;
    }
}
