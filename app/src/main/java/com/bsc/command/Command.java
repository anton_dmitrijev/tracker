package com.bsc.command;

public interface Command {
    void execute();
}
