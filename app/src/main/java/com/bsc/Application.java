package com.bsc;

import com.bsc.command.Command;
import com.bsc.command.executor.CommandExecutor;
import com.bsc.command.factory.CommandFactory;
import com.bsc.di.ApplicationModule;
import dagger.ObjectGraph;
import jline.console.ConsoleReader;

import javax.inject.Inject;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.bsc.command.factory.CommandFactory.CommandType.CONSOLE_INPUT;
import static com.bsc.command.factory.CommandFactory.CommandType.IMPORT_FILE;
import static com.bsc.command.factory.CommandFactory.CommandType.PRINT_BALANCE;

public class Application {
    @Inject
    CommandFactory factory;
    @Inject
    CommandExecutor commandExecutor;

    public static void main(String[] args) {
        ObjectGraph
                .create(ApplicationModule.class)
                .get(Application.class)
                .bootstrapApplication(args);
    }

    private void bootstrapApplication(String[] args) {
        if (args.length > 0) {
            Command importTransactionsFromFile = factory.createCommand(IMPORT_FILE, args[0]);
            commandExecutor.executeCommand(importTransactionsFromFile);
        }

        printBalanceEveryMinute();

        startConsoleApplication();
    }

    private void startConsoleApplication() {
        ConsoleReader console = createConsole();
        String line;
        try {
            while ((line = console.readLine()) != null) {
                commandExecutor.executeCommand(factory.createCommand(CONSOLE_INPUT, line));
            }
        } catch (IOException e) {
            throw new IllegalStateException("Unable to read from console!");
        }
    }

    private ConsoleReader createConsole() {
        try {
            ConsoleReader reader = new ConsoleReader();
            reader.setPrompt(">>>");
            return reader;
        } catch (IOException e) {
            throw new IllegalStateException("Unable to start the application!");
        }
    }

    private void printBalanceEveryMinute() {
        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(() -> {
            commandExecutor.executeCommand(factory.createCommand(PRINT_BALANCE, null));
        }, 0L, 10L, TimeUnit.SECONDS);
    }
}
