package com.bsc.util;

import com.bsc.domain.Transaction;

import java.math.BigDecimal;

public final class TransactionUtil {
    private static final String TRANSACTION_PATTERN = "[A-Z]{3} -?((0)|([1-9]\\d*))(\\.\\d{1,2})?";

    private TransactionUtil() {
    }

    public static boolean isValidTransactionImportLine(String line) {
        return line.matches(TRANSACTION_PATTERN);
    }

    public static Transaction parseTransaction(String line) {
        if (!isValidTransactionImportLine(line)) {
            return null;
        }

        String[] tokens = line.split(" ");
        return new Transaction(tokens[0], new BigDecimal(tokens[1]));
    }
}
