package com.bsc.util;

import com.bsc.domain.ExchangeRate;

import java.math.BigDecimal;

public final class ExchangeRateUtil {
    // http://www.quickmeme.com/meme/35wnxf
    private static final String RATE_PATTERN = "[A-Z]{3} [A-Z]{3} ((0\\.\\d{1,2})|([1-9]\\d*(\\.\\d{1,2})?))";

    private ExchangeRateUtil() {
    }

    public static boolean isValidExchangeRateCommand(String command) {
        return command.matches(RATE_PATTERN);
    }


    public static ExchangeRate parseExchangeRate(String command) {
        if (!isValidExchangeRateCommand(command)) {
            return null;
        }

        String[] tokens = command.split(" ");
        return new ExchangeRate(tokens[0], tokens[1], new BigDecimal(tokens[2]));
    }
}
